﻿#target Illustrator

/*
 * script.name = exportMultipleSetsLayersAsImages.jsx;
 * script.description = Export layers of each project in a chosen folder as PNG images;
 * script.requirements = Nothing.
 * script.elegant = false;
 */

/**
 * Export layers of each project in a chosen folder as PNG images.
 * Derived from the exportLayersAsImages script to process Ai projects in a folder.
 */


var RESULT_FOLDER_NAME = '_SETS_EXPORT';
var MAX_RENAMED_LAYERS = 30;


if (!String.prototype.includes)
{
    String.prototype.includes = function() {
        'use strict';
        return String.prototype.indexOf.apply(this, arguments) !== -1;
     }
}


main();


/**
 * Exports an Illustrator project composition and each of its layer as PNG images.
 *
 * This function processes only the document's visible layers.
 *
 * @param file File object representing an Illustrator project
 * @param name Name of the project image result
 * @param output_path Destination path of the project image result
 * @param layers_output_path Destination path of the individual layer images
 * @param hide_fx_layers Indicates whether FX layers must be hidden
 * @param auto_rename Indicates whether layer names invalid characters must be automatically replaced
 */
function export_layers(file, name, output_path, layers_output_path, hide_fx_layers, auto_rename) {

    // Open document in RGB color space
    var document = app.open(file, DocumentColorSpace.RGB);

    // Prevent spaces in set name
    name = document.name.split('.')[0].replace(/[\s()]/gi, '_');

    // Export set

    var options = getExportOptions(100.0, 100.0);
    // var document_name = document.name.split('.')[0];

    // Export layers

    const layer_regex = /^[\d]+_[\w]+$/gi;
    const invalid_chars = /[^\w]/gi;

    // Choose how to rename a layer's invalid name
    rename_layers(auto_rename);

    if (hide_fx_layers)
        hideLayers(/fx/i);

    var set_img = new File(output_path + '/' + name + '.png');
    document.exportFile(set_img, ExportType.PNG24, options);

    var sets_paths_file = new File(output_path + '/sets_paths.txt');

    sets_paths_file.encoding = 'UTF-8';
    sets_paths_file.open('a');
    sets_paths_file.write(set_img.fsName + ';' + document.fullName.fsName);


    var visible_layers = getVisibleLayers();
    var visible_count = visible_layers.length;

    // Create layers folder
    var layers_folder = new Folder(layers_output_path + '/' + name);

    if (!layers_folder.exists)
        layers_folder.create()

    var layers_paths_file = new File(layers_output_path + '/layers_paths.txt');
    layers_paths_file.encoding = 'UTF-8';
    layers_paths_file.open('a');

    hideLayers(null);

    var invalid_layer_names = false;

    for (var i = 0; i < visible_count; ++i)
    {
        var layer = document.layers[visible_layers[i]];
        layer.visible = true;

        // Export layer image
        var layer_path = layers_folder.fsName + '/' + name + '_' + layer.name;

        // while (File(layer_path + '.png').exists)
        // {
        //     $.writeln(layer_path + '.png');
        //     layer_path = path + '_' + i;
        //     i++;
        // }

        // Rename layer if another one with the same name already exists

        var layer_img = new File(layer_path + '.png');

        var i = 0;
        while(layer_img.exists === true) {
            layer_path = layers_folder.fsName + '/' + name + '_' + layer.name + '_' + toString(i);
            layer_img.changePath(layer_path + '.png');
            ++i;
            $.writeln(i);
        }


        document.exportFile(layer_img, ExportType.PNG24, options);

        // Export layer data
        layers_paths_file.write(layer_img.fsName + ';');

        layer.visible = false;

        var layer_prefix = layer.name.split('_')[0];
        invalid_layer_names = invalid_layer_names || !layer_prefix.match(/^\d+$/)
    }

    var invalid_layer_names_text;

    if (invalid_layer_names)
        invalid_layer_names_text = ';invalid_layer_names'
    else
        invalid_layer_names_text = ''

    sets_paths_file.writeln(invalid_layer_names_text)
    sets_paths_file.close();

    // Restore visible layers
    showLayers(visible_layers);

    layers_paths_file.writeln('')
    layers_paths_file.close();
    document.close(SaveOptions.DONOTSAVECHANGES);

    /**
     * Returns predefined options for PNG export.
     *
     * @param hscale Result's width scaling factor
     * @param vscale Result's height scaling factor
     */
    function getExportOptions(hscale, vscale)
    {
        var options = new ExportOptionsPNG24();
        var newRGBColor = new RGBColor();

        newRGBColor.red = 0;
        newRGBColor.green = 0;
        newRGBColor.blue = 0;
        options.matteColor = newRGBColor;
        options.antiAliasing = true;
        options.transparency = true;
        options.artBoardClipping = true;
        options.horizontalScale = hscale;
        options.verticalScale = vscale;

        return options
    }

    /**
     * Hides layers whose names match a given pattern.
     *
     * @param regex: The pattern in question
     */
    function hideLayers(regex)
    {
        if (regex != null)
        {
            fn = function(layer) {
                if (layer.name.match(regex))
                    layer.visible = false;
            };
        }
        else
        {
            fn = function(layer) {
                layer.visible = false;
            };
        }
        forEach(document.layers, fn);
    }

    /**
     * Shows a subset of layers in the active document.
     *
     * @param indices: Indices of the layers in the active document's layer array
     */
    function showLayers(indices)
    {
        forEach(indices, function(index) {
            document.layers[index].visible = true;
        });
    }

    /**
     * Returns the visible layers in the active document.
     *
     * @returns: The list of visible layer indices
     */
    function getVisibleLayers()
    {
        var layers = document.layers;
        var visible_layers = [];
        var n = layers.length;

        for (var i = 0; i < n; ++i)
        {
            if (layers[i].visible)
                visible_layers.push(i);
        }

        return visible_layers;
    }

    function rename_layers(auto_rename)
    {
        if (auto_rename)
        {
            forEach(document.layers, function(layer) {
                layer.name = layer.name.replace(invalid_chars, '_');
            });
        }
        else
        {
            forEach(document.layers, function(layer) {
                while (!layer_regex.test(layer.name))
                    layer.name = prompt("Layer name '" + layer.name + "' is invalid. Please rename it following the pattern ORDER_NAME, where ORDER is a series of digits, and NAME is composed of characters from 'a-z', 'A-Z', '0-9', '_' and '-'.", layer.name);
            });
        }
    }

    /**
     * Applies a function to each element of a given collection.
     */
    function forEach(collection, fn)
    {
        var n = collection.length;

        for (var i = 0; i < n; ++i)
        {
            fn(collection[i]);
        }
    }
}


function globFolder(folder, fn, extension, name) {
    var files = folder.getFiles();

    for (var i = 0; i < files.length; ++i)
    {
        current_file = files[i];

        if (current_file instanceof Folder)
        {
            var extended_name = name + '_' + current_file.displayName;

            globFolder(current_file, fn, extension, extended_name);
        }
        else
        {
            var file_and_ext = current_file.displayName.split('.');
            var old_folder_regex = /^old$/gi;

            if (file_and_ext[1] == extension
                && current_file.path.includes('ersion')
                && !old_folder_regex.test(current_file.parent.name)
                && !file_and_ext[0].slice(-2).match(/^zn$/i))
            {
                var extended_name = name + '_' + file_and_ext[0]
                fn(current_file, extended_name);
            }
        }
    }
}


function main() {
    // Disable alert boxes
    app.userInteractionLevel = UserInteractionLevel.DONTDISPLAYALERTS;

	var root_folder = Folder('S:/LIB/set_AI_BG/BG').selectDlg('Select a folder to glob');

    if (root_folder == null)
        return;

    // Create sets and layers folders
    var result_path = root_folder.fsName + '/' + RESULT_FOLDER_NAME;
    var sets_path = result_path + '/sets';
    var layers_path = result_path + '/layers'

    var folder = new Folder(result_path);

    if (!folder.exists)
        folder.create();

    folder.changePath(sets_path);

    if (!folder.exists)
        folder.create();

    folder.changePath(layers_path);

    if (!folder.exists)
        folder.create();

    // Replace invalid characters in layer names and hide FX layers
    var rename_layers = confirm("Automatically replace invalid characters in layer names by '_' ?");
    var hide_fx_layers = confirm("Hide FX layers ?");

    globFolder(root_folder, function(file, name) { export_layers(file, name, sets_path, layers_path, hide_fx_layers, rename_layers); }, 'ai', root_folder.displayName);
}

function main2() {
    // Disable alert boxes
    app.userInteractionLevel = UserInteractionLevel.DONTDISPLAYALERTS;

    var projects_paths = File.openDialog('S:/LIB/set_AI_BG/BG_v002/BG/_SETS_EXPORT_TEST/sets');
    projects_paths.open('r');

    while (!projects_paths.eof) {
        line = projects_paths.readln();
        var path = line.split(';')[1];
        var output_path = line.split(';')[0];
        $.writeln(path);
        var file = new File(path)

        export_layers(file, null, 'S:/LIB/set_AI_BG/BG_v002/BG/_SETS_EXPORT/sets', 'S:/LIB/set_AI_BG/BG_v002/BG/_SETS_EXPORT/layers', true, true);
    }

    projects_paths.close();
}