import os
import sys
import glob
import subprocess
import re


COMPOSITED_FOLDER = '/composited'
COMPARED_FOLDER = '/compared'


def usage():
    print(
        'USAGE\n',
        '\tcompareLayers <root_path> <result>'
    )


def composite(path, file_ext, output_path):
    layers_paths = glob.glob('{path}/*.{ext}'.format(path=path, ext=file_ext))
    layers_paths.reverse()

    # Get first layer dimensions
    layer_dims = subprocess.check_output(['magick', 'identify', '-format', '%[w]x%[h]', layers_paths[0]])

    args = ['magick', 'convert']
    args.extend(['-size', layer_dims.decode(), 'xc:white'])

    for path in layers_paths:
        args.extend([path, '-geometry', '+0+0', '-composite'])

    args += [output_path]

    subprocess.run(args,  check=True)


def mark_image(path, output_path):
    args = ['magick', 'convert']
    args += ['{}'.format(path)]

    set_dims = subprocess.check_output(['magick', 'identify', '-format', '%[w]x%[h]', path])
    set_width, set_height = set_dims.decode().split('x')

    args.extend(['-fill', 'rgba(0, 0, 0, 0.3)'])
    args.extend(['-draw', 'rectangle 0,{} {},{}'.format(int(set_height) - 16, int(set_width)-1, int(set_height) - 1)])
    args.extend(['-fill', 'rgba(0,0,0,0.3)'])

    args.extend(['-fill', 'white', '-pointsize', '16'])

    # Annotate path
    args.extend(['-gravity', 'SouthWest', '-annotate', '0', '{}'.format(path)])

    args.append('{}'.format(output_path))

    subprocess.run(args, check=True)


def diff(reference_path, composited_path, output_path):
    args = ['magick', 'compare']
    args.extend([reference_path, composited_path])
    args.append(output_path)

    # subprocess.run(args, check=True)
    cmd = ' '.join(args)
    print(cmd)
    os.system(cmd)

def compare(reference_path, composited_path, difference_path, output_path):
    pass


def process_compare(sets_path, layers_path, output_path):
    references_paths = glob.glob('{path}/*.png'.format(path=sets_path))
    sets_names = list(map(lambda path: os.path.splitext(os.path.basename(path))[0], references_paths))

    layer_folders = glob.glob(layers_path + '/*')
    set_count = len(references_paths)

    if not os.path.exists(output_path):
        os.mkdir(output_path)

    compositions_path = output_path + COMPOSITED_FOLDER
    differences_path = output_path + COMPARED_FOLDER

    if not os.path.exists(compositions_path):
        os.mkdir(compositions_path)

    if not os.path.exists(differences_path):
        os.mkdir(differences_path)

    for i in range(set_count):
        reference_path = references_paths[i].replace('/', '\\')
        composition_path = compositions_path + '/' + sets_names[i] + '.png'
        difference_path = differences_path + '/' + sets_names[i] + '.png'

        composition_path = composition_path.replace('/', '\\')
        difference_path = difference_path.replace('/', '\\')

        composite(layer_folders[i], 'png', composition_path)
        # mark_image(references_paths[i], reference_annotated_path)
        # mark_image(composited_sets[i], composited_annotated_path)

        diff(reference_path, composition_path, difference_path)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        usage()
        sys.exit(1)

    process_compare(sys.argv[1], sys.argv[2], sys.argv[3])
